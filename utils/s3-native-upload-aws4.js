"use strict";
const crypto = require('crypto');
const https = require("https");

function md5Hex(input) {
    return crypto.createHash('md5').update(input).digest('hex');
}

function sha256Hex(string) {
    return crypto.createHash('sha256').update(string, 'utf8').digest('hex');
}

function hmac(key, string) {
    return crypto.createHmac('sha256', key).update(string, 'utf8').digest();
}

function genAwsSignature(secretKey, datestamp, region, service) {
    let kDate = hmac("AWS4" + secretKey, datestamp);
    let kRegion = hmac(kDate, region);
    let kService = hmac(kRegion, service);
    let kSigning = hmac(kService, "aws4_request");
    return kSigning;
}

function AwsSign4(userOptions) {
    const options = userOptions;
    function createHttpsOption(postBodyStr) {
        const date = new Date(1520996769409);
        const amzdate = date.toISOString().replace(/[:\-]|\.\d{3}/g, '');
        const datestamp = date.toISOString().slice(0, 10).replace(/-/g, "");
        const payload_hash = sha256Hex(postBodyStr);

        const canonical_headers =
            'content-length:' + postBodyStr.length + '\n' +
            // 'content-type:' + options.content_type + '\n' +
            'host:' + options.hostname + '\n' +
            "x-amz-content-sha256:" + payload_hash + '\n'+
            'x-amz-date:' + amzdate + '\n';
        const signed_headers = 'content-length;host;x-amz-content-sha256;x-amz-date';
        const canonical_request = options.method + '\n' + options.canonical_uri + '\n' + options.canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash;

        const algorithm = 'AWS4-HMAC-SHA256';
        const credential_scope = datestamp + "/" + options.region + "/" + options.service + "/aws4_request";
        const string_to_sign = algorithm + '\n' + amzdate + '\n' + credential_scope + '\n' + sha256Hex(canonical_request);
        const signing_key = genAwsSignature(options.secretKey, datestamp, options.region, options.service);
        const signature = hmac(signing_key, string_to_sign).toString("hex");

        return {
            "method": options.method,
            "hostname": options.hostname,
            "path": options.canonical_uri,
            "headers": {
                'content-length': postBodyStr.length,
                // "content-type": options.content_type,
                "host": options.hostname,
                "authorization":
                    algorithm + ' ' +
                    "Credential=" + options.accessKey + "/" + credential_scope + ", " +
                    "SignedHeaders=" + signed_headers + ", " +
                    "Signature=" + signature,
                "x-amz-date": amzdate,
                "x-amz-content-sha256": payload_hash
            }
        };
    }
    this.makeHttpsRequest = function (postBodyStr, callback) {
        let httpsOptions = createHttpsOption(postBodyStr);
        let req = https.request(httpsOptions, function (res) {
            var chunks = [];
            res.on("data", function (chunk) {
                chunks.push(chunk);
            });
            res.on("end", function () {
                let body = Buffer.concat(chunks).toString();
                callback && callback(body);
            });
        });
        req.write(postBodyStr);
        req.end();
    }
}


const options = {
    accessKey: "",
    secretKey: "",
    hostname: "xx.s3.amazonaws.com",
    //// upload file path
    canonical_uri: "/xxx.txt",
    canonical_querystring: '',
    region: "ap-southeast-1",
    service: "s3",
    method: 'PUT',
    content_type: 'text/plain'
}

//// upload body or file content
let postBody = 'dfwgeghhhhhhhhhhhhhhhhhhhhhh';
let awsSign4 = new AwsSign4(options);
awsSign4.makeHttpsRequest(postBody, function (res) {
    console.log(res);
});


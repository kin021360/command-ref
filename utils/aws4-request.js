"use strict";

const AWS = require('aws-sdk');

//// For ES+IAM role
function EsConfig(Aws, EsHostname, EsIndex, EsRegion) {
    /*
     * The AWS credentials are picked up from the environment.
     * They belong to the IAM role assigned to the Lambda function.
     * Since the ES requests are signed using these credentials,
     * make sure to apply a policy that allows ES domain operations
     * to the role.
     * https://github.com/awslabs/amazon-elasticsearch-lambda-samples/blob/master/src/kinesis_lambda_es.js
     */
    const AWS = Aws;
    const esHostname = EsHostname;
    const esIndex = EsIndex;
    const esRegion = EsRegion;
    const awsEndpoint = new AWS.Endpoint(esHostname);
    const awsCredentials = new AWS.EnvironmentCredentials('AWS');
    const awsHttpClient = new AWS.NodeHttpClient();

    this.postToEsByIAMRole = function (bodyStr, callback) {
        let awsHttpRequest = new AWS.HttpRequest(awsEndpoint);
        awsHttpRequest.method = 'POST';
        awsHttpRequest.path = esIndex;
        awsHttpRequest.region = esRegion;
        awsHttpRequest.headers['presigned-expires'] = false;
        awsHttpRequest.headers['Host'] = awsEndpoint.host;
        awsHttpRequest.body = bodyStr;

        // console.log("-------end aws initalization-------");

        let awsSigner = new AWS.Signers.V4(awsHttpRequest, 'es');  // es: service code
        awsSigner.addAuthorization(awsCredentials, new Date());

        // console.log("-------end aws Signers-------");

        awsHttpClient.handleRequest(awsHttpRequest, null, function (res) {
            let body = '';
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function (chunk) {
                callback && callback(body);
            });
        }, function (err) {
            console.log('Error: ' + err);
            callback && callback(null);
        });
    }
}


//// For S3+Key+AWS4
function S3AWS4() {
    const awsEndpoint = new AWS.Endpoint("xx.s3.amazonaws.com");
    const awsCredentials = new AWS.Credentials('', '', null);
    const awsHttpClient = new AWS.NodeHttpClient();


    let awsHttpRequest = new AWS.HttpRequest(awsEndpoint);
    awsHttpRequest.method = 'PUT';
    awsHttpRequest.path = "/xxx.txt";
    awsHttpRequest.region = "ap-southeast-1";
    awsHttpRequest.headers['presigned-expires'] = false;
    awsHttpRequest.headers['Host'] = awsEndpoint.host;
    awsHttpRequest.headers['x-amz-content-sha256'] = 'a6c75ed9013e248f11e35691a4788c3d67d7eaf65daaf934d8b79ad8bb91df60';
    awsHttpRequest.body = 'dfwgeghhhhhhhhhhhhhhhhhhhhhh';

    // console.log("-------end aws initalization-------");

    let awsSigner = new AWS.Signers.V4(awsHttpRequest, 's3');  // es: service code
    awsSigner.addAuthorization(awsCredentials, new Date(1520911736097));

    // console.log("-------end aws Signers-------");

    awsHttpClient.handleRequest(awsHttpRequest, { proxy: "http://localhost:8888" }, function (res) {
        let body = '';
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', function (chunk) {
            console.log(body);
        });
    }, function (err) {
        console.log('Error: ' + err);
    });
}

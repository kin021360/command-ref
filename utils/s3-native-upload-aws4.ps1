function sha256Hex($_textToHash){
    $hasher = New-Object System.Security.Cryptography.SHA256Managed
    $toHash = [System.Text.Encoding]::UTF8.GetBytes($_textToHash)
    $hashByteArray = $hasher.ComputeHash($toHash)
    return [BitConverter]::ToString($hashByteArray).Replace('-','').ToLower()
}

function sha256HexFile($_path){
    $hashed = Get-FileHash -Path $_path -Algorithm SHA256
    return $hashed.hash.ToLower()
}

function hmac($_key,$_value){
    $hmacsha256 = New-Object System.Security.Cryptography.HMACSHA256
    if($_key -is [String]){
        $hmacsha256.key = [System.Text.Encoding]::UTF8.GetBytes($_key);
    }else{
        $hmacsha256.key = $_key
    }
    $buffer = $hmacsha256.ComputeHash([System.Text.Encoding]::UTF8.GetBytes($_value))
    return $buffer
}

function genSigningKey($_secretKey, $_date, $_region, $_service){
    $kDate = hmac "AWS4$_secretKey" $_date
    $kRegion = hmac $kDate $_region
    $kService = hmac $kRegion $_service
    $kSigning = hmac $kService 'aws4_request'
    return $kSigning
}

function getUTCDate{
    $utc = Get-Date;
    return $utc.ToUniversalTime().ToString('yyyyMMdd')
    #return '20180314'
}

function getAmzDate{
    $utc = Get-Date;
    return $utc.ToUniversalTime().ToString('yyyyMMddThhmmssZ')
    #return '20180314T030609Z'
}

function genCanonicalHeaders($_conentLength, $_hostName, $_contentSha256, $_amzDate){
    $array = @()
    $array += "content-length:$_conentLength"
    $array += "host:$_hostName"
    $array += "x-amz-content-sha256:$_contentSha256"
    $array += "x-amz-date:$_amzDate"
    return [String]::Join("`n", $array) + "`n"
}

function genCanonicalRequest($_method, $_canonicalUri, $_canonicalHeaders, $_signedHeaders, $_contentSha256){
    $array = @()
    $array += $_method
    $array += $_canonicalUri
    $array += ''
    $array += $_canonicalHeaders
    $array += $_signedHeaders
    $array += $_contentSha256
    return [String]::Join("`n", $array)
}

function genCredentialScope($_date, $_region, $_service){
    $array = @()
    $array += $_date
    $array += $_region
    $array += $_service
    $array += 'aws4_request'
    return [String]::Join('/', $array)
}

function genStringToSign($_algorithm, $_amzDate, $_credentialScope, $_canonicalRequest){
    $array = @()
    $array += $_algorithm
    $array += $_amzDate
    $array += $_credentialScope
    $array += sha256Hex $_canonicalRequest
    return [String]::Join("`n", $array)
}

function genAuthorization($_algorithm, $_accessKey, $_credentialScope, $_signedHeaders, $_signature){
    $array = @()
    $array += "$_algorithm Credential=$_accessKey/$_credentialScope"
    $array += "SignedHeaders=$_signedHeaders"
    $array += "Signature=$_signature"
    return [String]::Join(', ', $array)
}


#----------------------------------------Upload config----------------------------------------


#IAM user accessKey, secretKey
$accessKey = ''
$secretKey = ''

#S3 config
$bucketHostName = 'xx.s3.amazonaws.com'
$region = 'ap-southeast-1'
$service = 's3'
#upload file path
$canonicalUriPath = '/s3upload.txt'

#input file config
$filePath = 'C:\Users\nathanlam\Desktop\s3upload.txt'
$file = Get-Item -Path $filePath
$fileHash = sha256HexFile $filePath


#----------------------------------------Upload config end----------------------------------------


#constant
$signedHeaders = 'content-length;host;x-amz-content-sha256;x-amz-date'
$algo = 'AWS4-HMAC-SHA256'
$UTCDate = getUTCDate
$amzDate = getAmzDate

#generate hashing, signature...
$canonicalHeaders = genCanonicalHeaders $file.Length $bucketHostName $fileHash $amzDate
$canonicalRequest = genCanonicalRequest 'PUT' $canonicalUriPath $canonicalHeaders $signedHeaders $fileHash
$credentialScope = genCredentialScope $UTCDate $region $service
$stringToSign = genStringToSign $algo $amzDate $credentialScope $canonicalRequest
$signingKey = genSigningKey $secretKey $UTCDate $region $service
$signatureByte = hmac $signingKey $stringToSign
$signature = [BitConverter]::ToString($signatureByte).Replace('-','').ToLower()
$authorization = genAuthorization $algo $accessKey $credentialScope $signedHeaders $signature

# create headers
$reqHeaders = @{}
$reqHeaders.Add('content-length', $file.Length)
$reqHeaders.Add('host', $bucketHostName)
$reqHeaders.Add('authorization', $authorization)
$reqHeaders.Add('x-amz-date', $amzDate)
$reqHeaders.Add('x-amz-content-sha256', $fileHash)


#Write-Host $canonicalHeaders
#Write-Host -----------------------------
#Write-Host -----------------------------
#Write-Host $canonicalRequest
#Write-Host -----------------------------
#Write-Host -----------------------------
#Write-Host $credentialScope
#Write-Host -----------------------------
#Write-Host -----------------------------
#Write-Host $stringToSign
#Write-Host -----------------------------
#Write-Host -----------------------------
#Write-Host $signingKey
#Write-Host -----------------------------
#Write-Host -----------------------------
#Write-Host $signature
#Write-Host -----------------------------
#Write-Host -----------------------------
#Write-Host $authorization


#upload request
Invoke-WebRequest -Uri "https://$bucketHostName$canonicalUriPath" -Method Put -Headers $reqHeaders -InFile $filePath

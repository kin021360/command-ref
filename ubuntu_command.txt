sudo apt-get update

sudo apt-get autoremove --purge [package]

-----------------Install Go:--------------------------
# Download and unzip go1.18.2.linux-amd64.tar.gz
sudo mv go /usr/local

sudo nano ~/.profile
nano .bashrc
# Add the following lines
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH

go version
----------------------------------------------------------

-----------------Install Nodejs:--------------------------
sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

Update npm:
sudo npm install -g npm@latest
----------------------------------------------------------

-----------------sed regex--------------------------------
domain_tenant=($(sed -r -e 's/([a-z0-9]+)\.([a-z0-9]*)/ \1 \2 /g' <<< $i))
suffix=$(echo $suffix | sed 's/-//g')
e=$(nslookup "google.com" | grep "Address:" | sed -r -e 's/.*Address:  ([0-9]+.[0-9]+.[0-9]+.[0-9]+)/ \1 /g')
----------------------------------------------------------

sudo netstat -lnptu

# disable cloud-init
systemctl disable cloud-init-local cloud-init cloud-config cloud-final


Add volume:
fdisk /dev/xvdf
n
w

mkfs.ext3 /dev/xvdf1
mkdir /mnt/v13GB
chmod 777 /mnt/v13GB/
mount -t ext3 /dev/xvdf1 /mnt/v13GB
mkdir /mnt/v13GB/json
chmod 777 /mnt/v13GB/json/
umount /dev/xvdf1


Disk space:
df -h
du -sh ./*/
ll -h


Install old 7zip:
sudo apt-get install p7zip-full

Install new version 7z/(use of 7za command):
sudo apt-get update
sudo apt-get install -y dkms build-essential libncurses-dev autoconf automake libtool libssl-dev zlibc zlib1g zlib1g-dev
unzip p7zip_15.09.zip
cd p7zip_15.09
sudo make
sudo chmod 777 install.sh
sudo make install
7za

Extract 7zip file:
7za e -o/mnt/v13GB/json/ xxxx.7z

ls | wc -l


Install awscli:
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
export PATH=~/bin:$PATH
----------------------------------------------------
Extend LVM space

列出所有磁碟切割
sudo fdisk -l

產生新的磁碟切割
sudo fdisk /dev/sda

p >> 顯示所有切割

n >> 產生一個新的切割

p >> 選擇實體切割

3 >> 產生 /dev/sda3

enter >> 選擇起始區塊位址 (from last partition block)

enter >> 選擇結束區塊位址

t >> 轉換切割為 Linux LVM

8e >> 選擇切割為 Linux LVM

p  >> 顯示所有切割

w >> 執行切割異動，並離開

重新啟動系統
sudo reboot

建立 physical volume
sudo pvcreate /dev/sda3

將目前之 Volume Group 擴充到 physical volume，顯示VG Name，找出 Volume Group 與 VG Name，VG Name = Volume Group Name
sudo vgdisplay
sudo vgextend  ApaMoodle-pv /dev/sda3

擴充主要的 Logical Volume，先找出 LV Name，如 /dev/ApaMoodle-pv/root。然後擴充您所需要的 Logical Volume 大小 (GBs)。
sudo lvdisplay
sudo lvextend -L +250G  /dev/ApaMoodle-pv/root

調整檔案系統至新配置的空間
sudo resize2fs /dev/ApaMoodle-pv/root

檢查新檔案系統的大小
sudo df -hT

再次重新開機
sudo reboot

http://apacph168.pixnet.net/blog/post/217898139-%E5%B0%87vmware-esxi-v6%E4%B8%8A%E7%9A%84ubuntu-server-v16.04.2%E5%8A%A0%E5%A4%A7%E7%A3%81%E7%A2%9F%E7%A9%BA
----------------------------------------------------
#!/bin/bash
apt-get update
apt-get install -y apache2
apt-get install -y libapache2-mod-php5 php5
service apache2 restart


#!/bin/bash
apt-add-repository -y ppa:webupd8team/java
apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
apt-get install -y oracle-java8-installer
apt-get install -y unzip

#!/bin/bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-cache policy docker-ce
apt-get install -y docker-ce
curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
apt-get install -y unzip htop
----------------------------------------

rc.local
screen -dm /bin/bash -c "cd /home/ubuntu/talkleed_nodejs/apiGatewayV2.0/; npm start; /bin/bash"
screen -dm /bin/bash -c "sleep 300s; dd if=/dev/xvda1 of=/dev/null bs=32M; /bin/bash"
